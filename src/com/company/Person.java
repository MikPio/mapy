package com.company;

import java.util.List;

public class Person implements Comparable<Person>{

    String imie;
    Integer wiek;
    double wzrost;
    Sex plec;

    public int compareTo(Person otherPerson){
        return(this.wiek - otherPerson.wiek);
    }

    public Person(String imie, int wiek, double wzrost, Sex plec){

        this.imie = imie;
        this.wiek = wiek;
        this.wzrost = wzrost;
        this.plec = plec;

    }
}
