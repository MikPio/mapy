package com.company;

import java.util.Scanner;
import java.util.TreeMap;

public class Słownik {

    private TreeMap<String, String> słownik;

    public Słownik(){
        słownik = new TreeMap<String, String>();
        słownik.put("pies", "dog");
        słownik.put("kot", "cat");
        słownik.put("mysz", "mouse");
        słownik.put("krokodyl", "crocodile");
        słownik.put("tchórzofretka", "frenchman");

    }

    /** 1 */
    public void wyswietl(){
        for(String key : słownik.keySet()){
            System.out.println(key + " = " + słownik.get(key));
        }
    }

    /** 2 */
    public void wyszukaj(String polskie){
        for(String key : słownik.keySet()){
            if(słownik.get(key).compareTo(polskie) >= 0){
                System.out.println(key + " = " + słownik.get(key));
            }
        }
    }

    /** 3 */
    public void dodaj(String polskie, String angielskie){
        potwierdzenieOperacji("Dodano słowo", polskie, angielskie);
        słownik.put(polskie, angielskie);
    }

    /** 4 */
    public void edytuj(String polskie){
        Scanner sc = new Scanner(System.in);
        słownik.remove(polskie);

        System.out.println("Podaj nowe hasło po polsku: ");
        String nowePolskie = sc.nextLine();
        System.out.println("Podaj tłumaczenie na angielski: ");
        String noweAngielskie = sc.nextLine();

        słownik.put(nowePolskie, noweAngielskie);
        potwierdzenieOperacji("Zedytowano słowo", nowePolskie, noweAngielskie);
    }



    private void potwierdzenieOperacji(String komentarz, String polskie, String angielskie){
        System.out.println(komentarz + ": " + polskie + ", " + angielskie);
    }


}
