package com.company;

public class Player implements Comparable<Player>{

    int ranking;
    String name;
    int age;

    public int compareTo(Player otherPlayer){
        return(this.ranking - otherPlayer.ranking);
    }

    public Player(int ranking, String name, int age){
        this.ranking = ranking;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Player{" +
                "ranking=" + ranking +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
