package com.company;

public class BinarySearchClass {
    int low = 0;
    int high = 0;
    int mid = 0;


    public int binarySearch(int listData[], int value){
        low = 0;
        high = listData.length - 1;

        while(low <= high){
            mid = (low+high)/2;
            if(listData[mid] == value){
                return mid;
            }else if(listData[mid] < value){
                low = mid + 1;
            }else{
                high = mid -1;
            }
        }
        return -1;
    }


}
