package com.company;

import java.util.List;

public class IntBubbleSort implements BubbleSort {

    int[] table;

    IntBubbleSort(int[] table){
        this.table = table;

    }

    @Override
    public void sort(List list) {
        int elements = table.length;
        int temp;
        do {
            for (int i = 0; i < elements - 1; i++) {
                if (table[i] > table[i + 1]) {
                    temp = table[i];
                    table[i] = table[i + 1];
                    table[i + 1] = temp;
                }
            }
            elements = elements - 1;
        } while (elements > 1);
        for (int i = 0; i < table.length; i++) {
            System.out.print(table[i] + ", ");
        }
    }
}



